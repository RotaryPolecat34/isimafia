import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IObjet } from './objet';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ObjetService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private objetsUrl = 'https://isimafia.azurewebsites.net/api/Objet';

    getObjets(): Observable<IObjet[]> {
        return this.http.get<IObjet[]>(this.objetsUrl)
    }

    getObjet(id: number): Observable<IObjet> {
        const url = `${this.objetsUrl}/${id}`;
        return this.http.get<IObjet>(url);
    }

    updateObjet(objet: unknown, id: number): Observable<any> {
        const url = `${this.objetsUrl}/${id}`;
        return this.http.put(url, objet, this.httpOptions);
    }

    addObjet(objet: unknown): Observable<IObjet> {
        return this.http.post<IObjet>(this.objetsUrl, objet, this.httpOptions);
    }

    deleteObjet(objet: IObjet | number): Observable<IObjet> {
        const id = typeof objet === 'number' ? objet : objet.Id;
        const url = `${this.objetsUrl}/${id}`;

        return this.http.delete<IObjet>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { };

}

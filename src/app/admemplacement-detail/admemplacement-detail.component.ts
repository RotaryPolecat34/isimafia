import { Component, OnInit, Input } from '@angular/core';
import { IEmplacement } from '../emplacement';
import { EmplacementService } from '../emplacement.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-admemplacement-detail',
  templateUrl: './admemplacement-detail.component.html',
  styleUrls: ['./admemplacement-detail.component.scss']
})
export class AdmemplacementDetailComponent implements OnInit {

    @Input() emplacement: IEmplacement;

    constructor(
        private route: ActivatedRoute,
        private emplacementService: EmplacementService,
        private location: Location
    ) { }

    getEmplacement(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.emplacementService.getEmplacement(id)
            .subscribe(emplacement => this.emplacement = emplacement);
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.emplacementService.updateEmplacement(this.emplacement, id)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getEmplacement();
    }

    delete(emplacement: IEmplacement): void {
        this.emplacementService.deleteEmplacement(emplacement)
            .subscribe(() => this.goBack());
    }

    goBack(): void {
        this.location.back();
    }

}

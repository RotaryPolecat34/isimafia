import { Injectable } from '@angular/core';
import { IJoueur } from './joueur';
import { IGame } from './game';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

let game: IGame;

@Injectable({
	providedIn: 'root'
})

export class JoueurService {

	constructor(private http: HttpClient) { }

	httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

	private urljoueur = 'https://isimafia.azurewebsites.net/api/Joueur';

	addJoueur(joueur: unknown): Observable<IJoueur> {
		return this.http.post<IJoueur>('https://isimafia.azurewebsites.net/api/game/new', joueur, this.httpOptions);
	}


	getJoueurs(): Observable<IJoueur[]> {
		return this.http.get<IJoueur[]>(this.urljoueur);
	}

	deleteJoueur(joueur: IJoueur | number): Observable<IJoueur> {
		const id = typeof joueur === 'number' ? joueur : joueur.Id;
		const url = `${this.urljoueur}/${id}`;

		return this.http.delete<IJoueur>(url, this.httpOptions);
	}
}

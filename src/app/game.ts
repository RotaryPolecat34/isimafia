import { IJoueur } from './joueur';
import { IChapitre } from './chapitre';
import { IPersonnage } from './personnage';

export interface IGame {
    joueur: IJoueur,
    chapitreVM: IChapitre,
    message: string,
    monstre: IPersonnage,
    GameOver: string,
    Victoire: string
}

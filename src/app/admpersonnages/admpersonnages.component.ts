import { Component, OnInit } from '@angular/core';
import { IPersonnage } from '../personnage';
import { IObjet } from '../objet';
import { Sortilege } from '../sortilege';
import { PersonnageService } from '../personnage.service';
import { ObjetService } from '../objet.service';
import { SortilegeService } from '../sortilege.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';


@Component({
  selector: 'app-admpersonnages',
  templateUrl: './admpersonnages.component.html',
  styleUrls: ['./admpersonnages.component.scss']
})
export class AdmpersonnagesComponent implements OnInit {

    personnages: IPersonnage[];
    objets: IObjet[];
    sortileges: Sortilege[];
    types = [
        {
            id: 0,
            text: 'Monstre'
        },
        {
            id: 1,
            text: 'PNJ'
        }
    ];
    selectedObjets = [];
    selectedSortileges = [];
    selectedType = null;
    dropdownSettings: IDropdownSettings;
    dropdownSettings2: IDropdownSettings;
    pvForm: number;
    attaqueForm: number;
    defenseForm: number;
    probaForm: number;
    dialogueForm: string;
    nomForm: string;


    constructor(private personnageService: PersonnageService, private objetService: ObjetService, private sortilegeService: SortilegeService) { }

    getObjets(): void {
        this.objetService.getObjets()
            .subscribe(objets => this.objets = objets);
    }

    getSortileges(): void {
        this.sortilegeService.getSortileges()
            .subscribe(sortileges => this.sortileges = sortileges);
    }

    getPersonnages(): void {
        this.personnageService.getPersonnages()
            .subscribe(personnages => this.personnages = personnages);
    }

    add(): void {
       this. nomForm = this.nomForm.trim();
        if (!this.nomForm) { return; }
        var ObjetIds = new Array;
        var SortilegeIds = new Array;
        for (let item of this.selectedObjets) {
            ObjetIds.push(item.Id);
        }
        for (let item of this.selectedSortileges) {
            SortilegeIds.push(item.Id);
        }
        var personnage = { Nom: this.nomForm, Pv: this.pvForm, Attaque: this.attaqueForm, Defense: this.defenseForm, Dialogue: this.dialogueForm, Type: this.selectedType[0].id, Probabilite: this.probaForm, ObjetIds: ObjetIds, SortilegeIds:SortilegeIds}
        this.personnageService.addPersonnage(personnage)
            .subscribe(personnage => {
                location.reload();
            });
    }

    nullType(): void {
        this.selectedType = null;
    }

    ngOnInit() {
        this.getPersonnages();
        this.getObjets();
        this.getSortileges();
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'Id',
            textField: 'Nom',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.dropdownSettings2 = {
            singleSelection: true,
            idField: 'id',
            textField: 'text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

}

import { Component, OnInit } from '@angular/core';
import { JoueurService } from '../joueur.service';
import { IJoueur } from '../joueur';

@Component({
	selector: 'app-admjoueurs',
	templateUrl: './admjoueurs.component.html',
	styleUrls: ['./admjoueurs.component.scss']
})
export class AdmjoueursComponent implements OnInit {

	joueurs: IJoueur[];

	constructor(private joueurService: JoueurService) { }

	getJoueurs(): void {
		this.joueurService.getJoueurs()
			.subscribe(joueurs => this.joueurs = joueurs);
	}

	delete(joueur: IJoueur): void {
		this.joueurService.deleteJoueur(joueur)
			.subscribe(() => location.reload());
	}

	ngOnInit() {
		this.getJoueurs();
	}

}

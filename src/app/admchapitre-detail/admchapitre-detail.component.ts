import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ChapitreService } from '../chapitre.service';
import { IChapitre } from '../chapitre';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-admchapitre-detail',
  templateUrl: './admchapitre-detail.component.html',
  styleUrls: ['./admchapitre-detail.component.scss']
})
export class AdmchapitreDetailComponent implements OnInit {

    @Input() chapitre: IChapitre;
    dropdownSettings: IDropdownSettings;

    constructor(
        private route: ActivatedRoute,
        private chapitreService: ChapitreService,
        private location: Location) { }

    getChapitre(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.chapitreService.getChapitre(id)
            .subscribe(chapitre => {
                this.chapitre = chapitre;
            });
    }

    save(): void {
        var res;
        const id = +this.route.snapshot.paramMap.get('id');

        
        var chapitre = {
            Numero: this.chapitre.Numero,
            Histoire: this.chapitre.Histoire
        }
        this.chapitreService.updateChapitre(chapitre, id)
            .subscribe(() => this.goBack());
    }

    delete(chapitre: IChapitre): void {
        this.chapitreService.deleteChapitre(chapitre)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getChapitre();
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'Id',
            textField: 'Nom',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
}

goBack(): void {
    this.location.back();
}

}

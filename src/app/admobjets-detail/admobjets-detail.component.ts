import { Component, OnInit, Input } from '@angular/core';
import { IObjet } from '../objet';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ObjetService } from '../objet.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-admobjets-detail',
  templateUrl: './admobjets-detail.component.html',
  styleUrls: ['./admobjets-detail.component.scss']
})
export class AdmobjetsDetailComponent implements OnInit {

    @Input() objet: IObjet;
    types = [
        {
            id: 0,
            text: 'Consommable'
        },
        {
            id: 1,
            text: 'Equipement'
        }
    ];
    selectedType = new Array;
    dropdownSettings: IDropdownSettings;

    constructor(
        private route: ActivatedRoute,
        private objetService: ObjetService,
        private location: Location
    ) { }

    getObjet(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.objetService.getObjet(id)
            .subscribe(objet => {
                this.objet = objet;
                this.selectedType.push(this.types[objet.Type === 'Equipement' ? 1 : 0]);
            });
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        var objet = {Nom: this.objet.Nom, Puissance: this.objet.Puissance, Description:this.objet.Description, Url: this.objet.Description, Type : this.selectedType[0].text}
        this.objetService.updateObjet(objet, id)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getObjet();
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

    delete(objet: IObjet): void {
        this.objetService.deleteObjet(objet)
            .subscribe(() => this.goBack());
    }

    goBack(): void {
        this.location.back();
    }

    nullType(): void {
        this.selectedType = null;
    }
}

import { Component, OnInit } from '@angular/core';
import { IObjet } from '../objet';
import { ObjetService } from '../objet.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-admobjets',
  templateUrl: './admobjets.component.html',
  styleUrls: ['./admobjets.component.scss']
})
export class AdmobjetsComponent implements OnInit {

    objets: IObjet[];
    types= [
        {
            id: 0,
            text: 'Consommable'
        },
        {
            id: 1,
            text: 'Equipement'
        }
    ];
    selectedType = null;
    dropdownSettings: IDropdownSettings;
    constructor(private objetService: ObjetService) { }

    getObjets(): void {
        this.objetService.getObjets()
            .subscribe(objets => this.objets = objets);
    }

    add(nom: string, description: string, url: string, puissance: number): void {
        nom = nom.trim();
        if (!nom) { return; }
        var objet = {Nom:nom, Description:description, Url:url, Puissance:puissance, Type:this.selectedType[0].text}
        this.objetService.addObjet(objet)
            .subscribe(objet => {
                location.reload();
            });
    }

    nullType(): void {
        this.selectedType = null;
    }

    ngOnInit() {
        this.getObjets();
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

}

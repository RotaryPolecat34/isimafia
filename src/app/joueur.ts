import { IChapitre } from './chapitre';
import { IObjet } from './objet';
import { Sortilege } from './sortilege';
import { IEvenement } from './evenement';
import { IIndice } from './indice';
import { IPersonnage } from './personnage';

export interface IJoueur {
    Id: number,
    Nom: string,
    PV: number,
    Attaque: number,
    Defense: number,
    Evenements: IEvenement[],
    ObjetIds: number[],
    Objets: IObjet[],
    SortilegeIds: number[],
    Sortileges: Sortilege[],
    ChapitreIds: number [],
    Chapitres: IChapitre[],
    Indices: IIndice[]
}
